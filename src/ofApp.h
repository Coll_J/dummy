#pragma once

#include "ofMain.h"
#include "ofxDatGui.h"
#include <vector>
#include <iostream>

using namespace std;

class ofApp : public ofBaseApp{
	private:

	public:
		void setup();
		void update();
		void draw();

		void onDropdownEvent(ofxDatGuiDropdownEvent e);
		void onToggleEvent(ofxDatGuiToggleEvent e);
		void onSliderEvent(ofxDatGuiSliderEvent e);
		void onTextInputEvent(ofxDatGuiTextInputEvent e);

		ofxDatGui* gui = new ofxDatGui(100,100);

		vector<string> id_ = {"ID - 1", "ID - 2" , "ID - 3" ,"ID - 4"};
		vector<string> role_ = {"PENYERANG", "BERTAHAN", "GELANDANG", "ELSE"};
		vector<string> state_ = {"1 - CARI BOLA", "2 - FOLLOW BOLA", "3 - CARI GAWANG", "4 - PIVOT", 
								"5 - TENDANG", "ELSE"};
		vector<string> side_ = {"KIRI", "TENGAH", "KANAN"};
		vector<string> search_ = {"KICKOFF", "KICKING", "LAST SEEN", "PICKUP", "BACKWARD", "FREE",
								"FALLEN", "TO TARGET", "SPECIAL HOLD", "IN POSITION", "TO POSITION",
								"CHANGE POSITION", "FOR PASSING"};

		bool active_en;
		bool penal_en;
		bool detball_en;
		
		ofxDatGuiDropdown* search_dropdown;
		ofxDatGuiDropdown* id_dd;
		ofxDatGuiDropdown* role_dd;
		ofxDatGuiDropdown* state_dd;
		ofxDatGuiDropdown* side_dd;

		ofxDatGuiTextInput* team_id;

		ofxDatGuiToggle* active_;
		ofxDatGuiToggle* penal_;
		ofxDatGuiToggle* detball_;

		ofxDatGuiSlider* pan_;
		ofxDatGuiSlider* tilt_;
		ofxDatGuiSlider* direction_;
		ofxDatGuiSlider* kickdir_;
		ofxDatGuiSlider* posX_;
		ofxDatGuiSlider* posY_;
		ofxDatGuiSlider* ballX_;
		ofxDatGuiSlider* ballY_;

};
