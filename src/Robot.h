#pragma once

#include <string>
#include <vector>
#include "ofMain.h"
#include "ofxDatGui.h"
// #include "Listener.h"
// #include "mitecom-data.h"

class Robot {

private:

	ofPolyline* line;
	ofImage* imageNumber;
	ofImage* imageAttacker;
	ofImage* imageDefender;
	ofImage* imageMiddler;
	ofImage* imageAttackerDisabled;
	ofImage* imageDefenderDisabled;
	ofImage* imageMiddlerDisabled;
	ofImage* imageBall;
	ofImage* imageBallDisabled;
	ofImage* imageShortShoot;
	ofImage* imageLongShoot;

public:

	static std::vector<Robot*>* Instances;

	int id;
	int teamID;
	// std::string name;

	int active;
	int penalise;
	
	float active_sec_;

	uint8_t role;
	uint8_t side;
	uint8_t state;
	uint8_t search_;

	float pan;
	float tilt;

	float direction;
	float shootDirection;
	
	float positionX;
	float positionY;

	int detectBall;
	float _ballX;
	float _ballY;

	// int shootLength;
	// int kickCount;

	Robot (int id, int teamID);

	string getTeamString();
	int getRobotID();

	void setFromDropdown(int parent, int child);
	void setFromToggle(ofxDatGuiToggle* e, int check);
	void setFromSlider(ofxDatGuiSlider* e, float val);

	void setTeamID(string teamid_);
	// void setActive(bool act);
	// void setPenalise(bool pen);
	void setPan(float pan_);
	void setTilt(float tilt_);
	void setDirection(float dir_);
	void setShootDir(float shootdir_);
	void setPosX(float posX_);
	void setPosY(float posY_);
	// void setDetBall(bool detball_);
	void setBallX(float ballX_);
	void setBallY(float ballY_);

	// void Update (/*Listener* listener*/);
	void Reset ();
};