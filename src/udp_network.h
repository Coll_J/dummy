#ifndef NETWORK_UDP_NETWORK
#define NETWORK_UDP_NETWORK

namespace Network
{
    class UDPNetwork
    {
    private:

        int socket_;
        int receive_port_;
        int broadcast_port_;

    public:

        UDPNetwork();
        ~UDPNetwork();

        bool openPort(int port);
        bool openPort(int receive_port, int broadcast_port);
        bool closePort();

        // int receive(void *buffer, unsigned int size);
        bool broadcast(const void *data, unsigned int length);
    };
}

#endif