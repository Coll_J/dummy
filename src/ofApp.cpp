#include "ofApp.h"
#include "Robot.h"
#include "mitecom.h"

Robot* rob = new Robot(1,7);
Mitecom* mite = new Mitecom(rob);
//--------------------------------------------------------------
void ofApp::setup(){
    ofSetWindowPosition(0, 0);
    gui->setPosition(2,2);

    int tempid = rob->getRobotID();
    id_dd = gui->addDropdown("ROBOT ID", id_);
    id_dd->select(tempid-1);
    gui->addBreak()->setHeight(10.0f);

    string temp = rob->getTeamString();
    team_id = gui->addTextInput("TEAM ID");
    team_id->setText(temp);
    gui->addBreak()->setHeight(10.0f);
    
    active_ = gui->addToggle("ACTIVE", active_en);
    gui->addBreak()->setHeight(10.0f);
    
    role_dd = gui->addDropdown("ROLE", role_);
    gui->addBreak()->setHeight(10.0f);
    
    penal_ = gui->addToggle("PENALISE", penal_en);
    gui->addBreak()->setHeight(10.0f);
    
    side_dd = gui->addDropdown("SIDE", side_);
    gui->addBreak()->setHeight(10.0f);
    
    state_dd = gui->addDropdown("STATE", state_);
    gui->addBreak()->setHeight(10.0f);
    
    search_dropdown = gui->addDropdown("SEARCH", search_);
    gui->addBreak()->setHeight(10.0f);

    pan_ = gui->addSlider("PAN", -100.0, 100.0);
    gui->addBreak()->setHeight(10.0f);
    
    tilt_ = gui->addSlider("TILT", -100.0, 100.0);
    gui->addBreak()->setHeight(10.0f);

    direction_ = gui->addSlider("DIRECTION", -180.0, 180.0);
    gui->addBreak()->setHeight(10.0f);

    kickdir_ = gui->addSlider("KICK DIRECTION", -180.0, 180.0);
    gui->addBreak()->setHeight(10.0f);
    
    posX_ = gui->addSlider("POSITION X", 0.0, 900.0);
    gui->addBreak()->setHeight(10.0f);
    
    posY_ = gui->addSlider("POSITION Y", 0.0, 600.0);
    gui->addBreak()->setHeight(10.0f);
    
    detball_ = gui->addToggle("DETECT BALL", detball_en);
    gui->addBreak()->setHeight(10.0f);

    ballX_ = gui->addSlider("BALL X", 0.0, 900.0);
    gui->addBreak()->setHeight(10.0f);
    
    ballY_ = gui->addSlider("BALL Y", 0.0, 600.0);
    gui->addBreak()->setHeight(10.0f);

    // printf("test: %d\n", search_dropdown->size());
    gui->onDropdownEvent(this, &ofApp::onDropdownEvent);
    gui->onToggleEvent(this, &ofApp::onToggleEvent);
	gui->onSliderEvent(this, &ofApp::onSliderEvent);
	gui->onTextInputEvent(this, &ofApp::onTextInputEvent);

    mite->start();
}
//--------------------------------------------------------------
void ofApp::update(){
    // system("clear");
    gui->update();
    // printf("test: %d\n", search_opt);
    // cout << search_opt << endl;
}

//--------------------------------------------------------------
void ofApp::draw(){
    gui->setPosition(2,2);
    gui->draw();
    search_dropdown->draw();
}

void ofApp::onDropdownEvent(ofxDatGuiDropdownEvent e)
{
    cout << "parent: " << e.parent << " child: " << e.child << endl;
    rob->setFromDropdown(e.parent, e.child);
}

void ofApp::onToggleEvent(ofxDatGuiToggleEvent e)
{
    // if (e.target->is("toggle fullscreen")) toggleFullscreen();
    cout << "onToggleEvent: " << e.target->getLabel() << " " << e.checked << endl;
    rob->setFromToggle(e.target, e.checked);
}

void ofApp::onSliderEvent(ofxDatGuiSliderEvent e)
{
    cout << "onSliderEvent: " << e.target->getLabel() << " " << e.target->getValue() << endl;
    rob->setFromSlider(e.target, e.target->getValue());
    // if (e.target->is("datgui opacity")) gui->setOpacity(e.scale);
}

void ofApp::onTextInputEvent(ofxDatGuiTextInputEvent e)
{
    cout << "onTextInputEvent: " << e.target->getLabel() << " " << e.target->getText() << endl;
    rob->setTeamID(e.target->getText());
}