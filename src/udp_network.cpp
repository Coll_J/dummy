#include "udp_network.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <fcntl.h>

using namespace Network;

UDPNetwork::UDPNetwork()
{
    socket_ = -1;
    receive_port_ = -1;
    broadcast_port_ = -1;
}

UDPNetwork::~UDPNetwork()
{
    closePort();
}

bool UDPNetwork::openPort(int port)
{
    return openPort(port, port);
}

bool UDPNetwork::openPort(int receive_port, int broadcast_port)
{
    receive_port_ = receive_port;
    broadcast_port_ = broadcast_port;

    socket_ = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket_ < 0)
    {
		fprintf(stderr, "Failure creating socket\n");
		return false;
	}

    int broadcast = 1;
	setsockopt(socket_, SOL_SOCKET, SO_BROADCAST, (void *) &broadcast, sizeof(broadcast));


	int fcntl_flags = fcntl(socket_, F_GETFL, 0);
	fcntl(socket_, F_SETFL, fcntl_flags | O_NONBLOCK);

	// struct sockaddr_in sa = { 0 };
	// sa.sin_family      = AF_INET;
	// sa.sin_addr.s_addr = htonl(INADDR_ANY);
	// sa.sin_port        = htons(receive_port_);

	// if ( bind(socket_, (struct sockaddr *) &sa, sizeof(sa)) < 0 ) {
	// 	fprintf(stderr, "Failure binding socket to port %d\n", receive_port_);
	// 	closePort();
	// 	return false;
	// }

    return true;
}

bool UDPNetwork::closePort()
{
    if (socket_ >= 0)
    {
        close(socket_);
        socket_ = 0;
    }

    return true;
}


bool UDPNetwork::broadcast(const void *data, unsigned int length)
{
    if (socket < 0)
        return false;

    struct ifaddrs *ifap;
    if (getifaddrs(&ifap) != 0)
        return false;

    for (struct ifaddrs *p = ifap; p != NULL; p = p->ifa_next)
    {
        if (p->ifa_addr == NULL)
            continue;

        if (p->ifa_addr->sa_family != AF_INET)
            continue;

        uint32_t interface_addr = ntohl(((struct sockaddr_in *)(p->ifa_addr))->sin_addr.s_addr);
        uint32_t broadcast_addr = ((struct sockaddr_in *)(p->ifa_broadaddr))->sin_addr.s_addr;

        if (interface_addr > 0 && interface_addr != 0x7F000001)
        {
            struct sockaddr_in recipient = { 0 };
            recipient.sin_family      = AF_INET;
            recipient.sin_port        = htons(broadcast_port_);
            recipient.sin_addr.s_addr = broadcast_addr;

            sendto(socket_, data, length, 0, (const struct sockaddr*)&recipient, sizeof recipient);
        }
    }

    freeifaddrs(ifap);
    return true;
}