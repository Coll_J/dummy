#include "mitecom.h"

#include <unistd.h>
#include <string.h>

#define MITECOM_PORT 4003

Mitecom::Mitecom(Robot* robo) : Network::UDPNetwork()
{
	robo_ = robo;
	// soccer_ = soccer;
	// robo_ = soccer_->getCurrentPlayer();
}

Mitecom::~Mitecom()
{
	thread_handler_ = false;
}

bool Mitecom::start()
{
	if (!openPort(MITECOM_PORT))
		return false;

	if (thread_handler_ == true)
		return false;

	thread_handler_ = true;
	return pthread_create(&thread_, NULL, &threadMethod, this);
}

void Mitecom::process()
{
	// MessageData message_data;
	MessageData return_data;

	while (thread_handler_)
	{
		return_data.header = 0x4354584D;
		return_data.team_id = robo_->teamID;
		return_data.robot_id = robo_->id;
		return_data.version = 1;

		int i = 0;
		return_data.values[i].key = KEY_ICHIRO_ROBOT_ACTIVE;
		return_data.values[i++].value = robo_->active;

		return_data.values[i].key = KEY_ICHIRO_ROBOT_ACTIVE_SEC;
		memcpy(&(return_data.values[i++].value), &(robo_->active_sec_), sizeof(float));

		return_data.values[i].key = KEY_ICHIRO_ROBOT_PENALISE;
		return_data.values[i++].value = robo_->penalise;

		return_data.values[i].key = KEY_ICHIRO_ROBOT_ROLE;
		return_data.values[i++].value = robo_->role;

		return_data.values[i].key = KEY_ICHIRO_ROBOT_SIDE;
		return_data.values[i++].value = robo_->side;

		return_data.values[i].key = KEY_ICHIRO_ROBOT_STATE;
		return_data.values[i++].value = robo_->state;

		return_data.values[i].key = KEY_ICHIRO_ROBOT_SEARCH;
		return_data.values[i++].value = robo_->search_;

		return_data.values[i].key = KEY_ICHIRO_ROBOT_PAN;
		memcpy(&(return_data.values[i++].value), &(robo_->pan), sizeof(float));

		return_data.values[i].key = KEY_ICHIRO_ROBOT_TILT;
		memcpy(&(return_data.values[i++].value), &(robo_->tilt), sizeof(float));

		return_data.values[i].key = KEY_ICHIRO_ROBOT_DIRECTION;
		memcpy(&(return_data.values[i++].value), &(robo_->direction), sizeof(float));

		return_data.values[i].key = KEY_ICHIRO_ROBOT_KICK_DIRECTION;
		memcpy(&(return_data.values[i++].value), &(robo_->shootDirection), sizeof(float));

		return_data.values[i].key = KEY_ICHIRO_ROBOT_POSITION_X;
		memcpy(&(return_data.values[i++].value), &(robo_->positionX), sizeof(float));

		return_data.values[i].key = KEY_ICHIRO_ROBOT_POSITION_Y;
		memcpy(&(return_data.values[i++].value), &(robo_->positionY), sizeof(float));

		return_data.values[i].key = KEY_ICHIRO_ROBOT_DETECT_BALL;
		return_data.values[i++].value = robo_->detectBall;

		if (robo_->detectBall)
		{
			return_data.values[i].key = KEY_ICHIRO_ROBOT_BALL_X;
			memcpy(&(return_data.values[i++].value), &(robo_->_ballX), sizeof(float));

			return_data.values[i].key = KEY_ICHIRO_ROBOT_BALL_Y;
			memcpy(&(return_data.values[i++].value), &(robo_->_ballY), sizeof(float));
		}

		return_data.length = i;
		// printf("length: %d\n", i);
		broadcast(&return_data, sizeof(return_data));

		usleep(30000);
	}
	closePort();
}

void *Mitecom::threadMethod(void *object)
{
    reinterpret_cast<Mitecom *>(object)->process();
    return 0;
}
