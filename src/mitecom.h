#ifndef SOCCER_MITECOM_H
#define SOCCER_MITECOM_H

#include "udp_network.h"
#include "Robot.h"
// #include "soccer/soccer.h"

#include <stdint.h>
#include <pthread.h>

class Mitecom : public Network::UDPNetwork
{
public:

    static const uint32_t RANGE_STATE           = 0x00000000;
    static const uint32_t RANGE_COGNITION       = 0x00010000;
    static const uint32_t RANGE_CAPABILITIES    = 0x00020000;
    static const uint32_t RANGE_STRATEGIES      = 0x00030000;
    static const uint32_t RANGE_USERDEFINED     = 0x10000000;

    enum
    {
        ROLE_IDLING     = 0,
        ROLE_OTHER      = 1,
        ROLE_STRIKER    = 2,
        ROLE_SUPORTER   = 3,
        ROLE_DEFENDER   = 4,
        ROLE_GOALIE     = 5
    };

    enum
    {
        ACTION_UNDEFINED        = 0,
        ACTION_POSITIONING      = 1,
        ACTION_GOING_TO_BALL    = 2,
        ACTION_TRYING_TO_SCORE  = 3,
        ACTION_WAITING          = 4
    };

    enum
    {
        STATE_INACTIVE  = 0,
        STATE_ACTIVE    = 1,
        STATE_PENALIZED = 2
    };

    enum
    {
        SIDE_UNSPECIFIED    = 0,
        SIDE_LEFT           = 1,
        SIDE_MIDDLE         = 2,
        SIDE_RIGHT          = 3
    };

    enum
    {
        KEY_ROBOT_CURRENT_ROLE          = RANGE_STATE + 0,
        KEY_ROBOT_CURRENT_ACTION        = RANGE_STATE + 1,
        KEY_ROBOT_CURRENT_STATE         = RANGE_STATE + 2,

        KEY_ROBOT_ABSOLUTE_X            = RANGE_COGNITION + 0,
        KEY_ROBOT_ABSOLUTE_Y            = RANGE_COGNITION + 1,
        KEY_ROBOT_ABSOLUTE_ORIENTATION  = RANGE_COGNITION + 2,
        KEY_ROBOT_ABSOLUTE_BELIEF       = RANGE_COGNITION + 3,

        KEY_BALL_RELATIVE_X             = RANGE_COGNITION + 4,
        KEY_BALL_RELATIVE_Y             = RANGE_COGNITION + 5,
        KEY_BALL_VELOCITY               = RANGE_COGNITION + 31,
        KEY_BALL_BELIEF                 = RANGE_COGNITION + 6,

        KEY_OPPGOAL_RELATIVE_X          = RANGE_COGNITION + 7,
        KEY_OPPGOAL_RELATIVE_Y          = RANGE_COGNITION + 8,
        KEY_OPPGOAL_RELATIVE_BELIEF     = RANGE_COGNITION + 9,

        KEY_OPPONENT_ROBOT_A_X          = RANGE_COGNITION + 10,
        KEY_OPPONENT_ROBOT_A_Y          = RANGE_COGNITION + 11,
        KEY_OPPONENT_ROBOT_A_BELIEF     = RANGE_COGNITION + 12,

        KEY_OPPONENT_ROBOT_B_X          = RANGE_COGNITION + 13,
        KEY_OPPONENT_ROBOT_B_Y          = RANGE_COGNITION + 14,
        KEY_OPPONENT_ROBOT_B_BELIEF     = RANGE_COGNITION + 15,

        KEY_OPPONENT_ROBOT_C_X          = RANGE_COGNITION + 16,
        KEY_OPPONENT_ROBOT_C_Y          = RANGE_COGNITION + 17,
        KEY_OPPONENT_ROBOT_C_BELIEF     = RANGE_COGNITION + 18,

        KEY_OPPONENT_ROBOT_D_X          = RANGE_COGNITION + 19,
        KEY_OPPONENT_ROBOT_D_Y          = RANGE_COGNITION + 20,
        KEY_OPPONENT_ROBOT_D_BELIEF     = RANGE_COGNITION + 21,

        KEY_TEAM_ROBOT_A_X              = RANGE_COGNITION + 22,
        KEY_TEAM_ROBOT_A_Y              = RANGE_COGNITION + 23,
        KEY_TEAM_ROBOT_A_BELIEF         = RANGE_COGNITION + 24,

        KEY_TEAM_ROBOT_B_X              = RANGE_COGNITION + 25,
        KEY_TEAM_ROBOT_B_Y              = RANGE_COGNITION + 26,
        KEY_TEAM_ROBOT_B_BELIEF         = RANGE_COGNITION + 27,

        KEY_TEAM_ROBOT_C_X              = RANGE_COGNITION + 28,
        KEY_TEAM_ROBOT_C_Y              = RANGE_COGNITION + 29,
        KEY_TEAM_ROBOT_C_BELIEF         = RANGE_COGNITION + 30,

        KEY_ROBOT_AVG_WALKING_SPEED     = RANGE_CAPABILITIES + 1,
        KEY_ROBOT_TIME_POSITION_BALL    = RANGE_CAPABILITIES + 2,
        KEY_ROBOT_MAX_KICKING_DISTANCE  = RANGE_CAPABILITIES + 3,

        KEY_OFFENSIVE_SIDE              = RANGE_STRATEGIES + 1,
        KEY_WALKING_TO_X_ABSOLUTE       = RANGE_STRATEGIES + 2,
        KEY_WALKING_TO_Y_ABSOLUTE       = RANGE_STRATEGIES + 3,
        KEY_SHOOTING_TO_X_ABSOLUTE      = RANGE_STRATEGIES + 4,
        KEY_SHOOTING_TO_Y_ABSOLUTE      = RANGE_STRATEGIES + 5,

        KEY_ICHIRO_ROBOT_ACTIVE         = RANGE_USERDEFINED + 0,
        KEY_ICHIRO_ROBOT_ACTIVE_SEC     = RANGE_USERDEFINED + 1,
        KEY_ICHIRO_ROBOT_PENALISE       = RANGE_USERDEFINED + 2,
        KEY_ICHIRO_ROBOT_ROLE           = RANGE_USERDEFINED + 3,
        KEY_ICHIRO_ROBOT_SIDE           = RANGE_USERDEFINED + 4,
        KEY_ICHIRO_ROBOT_STATE          = RANGE_USERDEFINED + 5,
        KEY_ICHIRO_ROBOT_SEARCH         = RANGE_USERDEFINED + 6,
        KEY_ICHIRO_ROBOT_PAN            = RANGE_USERDEFINED + 7,
        KEY_ICHIRO_ROBOT_TILT           = RANGE_USERDEFINED + 8,
        KEY_ICHIRO_ROBOT_DIRECTION      = RANGE_USERDEFINED + 9,
        KEY_ICHIRO_ROBOT_KICK_DIRECTION = RANGE_USERDEFINED + 10,
        KEY_ICHIRO_ROBOT_POSITION_X     = RANGE_USERDEFINED + 11,
        KEY_ICHIRO_ROBOT_POSITION_Y     = RANGE_USERDEFINED + 12,
        KEY_ICHIRO_ROBOT_DETECT_BALL    = RANGE_USERDEFINED + 13,
        KEY_ICHIRO_ROBOT_BALL_X         = RANGE_USERDEFINED + 14,
        KEY_ICHIRO_ROBOT_BALL_Y         = RANGE_USERDEFINED + 15
    };

    struct KeyValData
    {
        uint32_t key;
        int32_t value;
    };

    struct MessageData
    {
        uint32_t header;
        uint16_t version;
        uint16_t length;
        uint32_t flags;
        uint16_t team_id;
        uint16_t robot_id;
        KeyValData values[32];
    };

private:

    pthread_t thread_;
    bool thread_handler_;
    
    // Soccer *soccer_;
    // Player *current_player_;
    Robot* robo_;

    static void *threadMethod(void *object);

public:

    Mitecom(Robot* robo);
    ~Mitecom();

    bool start();
    bool stop() { return (thread_handler_ = false); }
    bool isActive() { return thread_handler_; }
    void process();
};

#endif