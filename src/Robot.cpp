#include "stdio.h"
#include "stdlib.h"
#include "Robot.h"

std::vector<Robot*>* Robot::Instances;

Robot::Robot (int id, int teamID) {

	this->id = id;
	this->teamID = teamID;

	Reset ();

	// line = new ofPolyline ();
}

string Robot::getTeamString()
{
	int temp = this->teamID;
	string temps = to_string(temp);
	return temps;
}

int Robot::getRobotID()
{
	return this->id;
}

void Robot::setTeamID(string teamid_)
{
	int temp = stoi(teamid_);
	this->teamID = temp;
	return;
}

void Robot::setFromDropdown(int parent, int child)
{
	if(parent == 0) this->id = child+1;
    if(parent == 6) this->role = child;
    if(parent == 10) this->side = child;
    if(parent == 12) this->state = child+1;
    if(parent == 14) this->search_ = child;

	return;
}

void Robot::setFromToggle(ofxDatGuiToggle* e, int check)
{
	if(e->is("ACTIVE")) 
	{
		this->active = check;
		printf("success toggle active with value %d!\n", check);
	}
	if(e->is("PENALISE")) 
	{
		this->penalise = check;
		printf("success toggle penalise with value %d!\n", check);
	}
	if(e->is("DETECT BALL")) 
	{
		this->detectBall = check;
		printf("success toggle ball detection with value %d!\n", check);
	}

	return;
}

void Robot::setFromSlider(ofxDatGuiSlider* e, float val)
{
	if(e->is("PAN")) 
	{
		this->pan = val;
		printf("success set pan to %f\n", val);
	}
	if(e->is("TILT")) 
	{
		this->tilt = val;
		printf("success set tilt to %f\n", val);
	}
	if(e->is("DIRECTION")) 
	{
		this->direction = val;
		printf("success set direction to %f\n", val);
	}
	if(e->is("KICK DIRECTION")) 
	{
		this->shootDirection = val;
		printf("success set kick direction to %f\n", val);
	}
	if(e->is("POSITION X")) 
	{
		this->positionX = val;
		printf("success set posX to %f\n", val);
	}
	if(e->is("POSITION Y")) 
	{
		this->positionY = val;
		printf("success set posY to %f\n", val);
	}
	if(e->is("BALL X")) 
	{
		this->_ballX = val;
		printf("success set ballX to %f\n", val);
	}
	if(e->is("BALL Y")) 
	{
		this->_ballY = val;
		printf("success set ballY to %f\n", val);
	}

	return;
}
	
void Robot::Reset () {
	this->active = 0;
	this->penalise = 0;
	
	// kepake, menunjukkan lama robot punya role itu, buat nentuin siapa yang pantas dapet role itu
	// makin lama makin pantas
	this->active_sec_ = 9999999999999;

	this->role = 0;
	this->side = 0;
	this->state = 0;
	this->search_ = 0;

	this->pan = 0.0;
	this->tilt = 0;

	this->direction = 0;
	this->shootDirection = 0;

	this->positionX = -1;
	this->positionY = -1;

	this->detectBall = 0;
	this->_ballX = -1;
	this->_ballY = -1;

	// shootLength = 0;
	// kickCount = 0;
}